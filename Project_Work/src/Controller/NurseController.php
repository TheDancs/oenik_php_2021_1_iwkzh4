<?php


namespace App\Controller;


use App\Models\AppointmentListModel;
use App\Models\HospitalSelectionModel;
use App\Repository\AppointmentRepository;
use App\Repository\HospitalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NurseController extends AbstractController
{
    private AppointmentRepository $appointmentRepository;
    private HospitalRepository $hospitalRepository;

    /**
     * NurseController constructor.
     * @param AppointmentRepository $appointmentRepository
     * @param HospitalRepository $hospitalRepository
     */
    public function __construct(AppointmentRepository $appointmentRepository, HospitalRepository $hospitalRepository)
    {
        $this->appointmentRepository = $appointmentRepository;
        $this->hospitalRepository = $hospitalRepository;
    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/nurse", name="nurse", methods={"GET", "POST"})
     */
    public function index(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_NURSE");

        $selectedHospital = intval($request->request->get('hospital_input'));

        $queryParam = $this->getv('hospitalId', 'false');
        if ($queryParam != 'false') {
            $selectedHospital = intval($queryParam);
        }

        list($hospitalList, $appointments) = $this->getTodayView($selectedHospital);

        return $this->render('nurse/nurse.html.twig', ["appointments" => $appointments, "hospitalSelection" => $hospitalList]);
    }

    function getv($key, $default = '', $data_type = '')
    {
        $param = ($_REQUEST[$key] ?? $default);

        if (!is_array($param) && $data_type == 'int') {
            $param = intval($param);
        }

        return $param;
    }

    /**
     * @param int $appointment
     * @return Response
     * @Route("/nurse/appointment/{appointment}/done", name="nurse_vaccination", requirements={"appointment": "\d+"}, methods={"GET"})
     */
    public function vaccineReceived(int $appointment): Response
    {
        $this->denyAccessUnlessGranted("ROLE_NURSE");

        $appointmentEntity = $this->appointmentRepository->findById($appointment);
        if ($appointmentEntity) {
            $appointmentEntity->setReceived(true);
            $this->appointmentRepository->updateEntity($appointmentEntity);
        }

        return $this->redirectToRoute('nurse', ['hospitalId' => $appointmentEntity->getHospital()->getHospitalId()]);
    }

    /**
     * @param int $selectedHospital
     * @return array[]
     */
    private function getTodayView(int $selectedHospital): array
    {
        $appointments = array();
        $hospitalList = array();

        $hospitals = $this->hospitalRepository->list();

        if (!$selectedHospital && count($hospitals) > 0) {
            $selectedHospital = $hospitals[0]->getHospitalId();
        }

        $filteredAppointments = $this->appointmentRepository->listByBookedDateAndHospital(new \DateTime('now'), $selectedHospital);
        foreach ($hospitals as $hospital) {
            array_push($hospitalList, new HospitalSelectionModel($hospital->getHospitalId(), $hospital->getName(), $hospital->getHospitalId() == $selectedHospital));
        }
        foreach ($filteredAppointments as $appointment) {
            $diseases = $appointment->getPatient()->getKnownDiseases();
            array_push($appointments, new AppointmentListModel($appointment->getAppointmentId(), $appointment->getPatient()->getFullName(), $appointment->getPatient()->getTaj(), $appointment->getPatient()->getAge(), $appointment->getPatient()->getAddress(), $diseases ? $diseases : "none", $appointment->getVaccine()->getName(), $appointment->isReceived()));
        }
        return array($hospitalList, $appointments);
    }
}