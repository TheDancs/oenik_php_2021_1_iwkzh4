<?php


namespace App\Controller;


use App\Service\SecurityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private SecurityService $securityService;

    /**
     * SecurityController constructor.
     * @param SecurityService $securityService
     */
    public function __construct(SecurityService $securityService)
    {
        $this->securityService = $securityService;
    }

    /**
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @Route(path="/login", name="app_login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('security/login.html.twig', ["authError" => $authenticationUtils->getLastAuthenticationError()]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(path="/logout", name="app_logout")
     */
    public function logoutAction(Request $request): Response
    {
        //not executed
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(path="/change", name="app_change_pwd", methods={"POST", "GET"})
     */
    public function changePwdAction(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $user = $this->getUser();
        $passwd = filter_var($request->request->get("password_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        if ($passwd) {
            $this->securityService->changePassword($user->getUsername(), $passwd);
            return $this->redirectToRoute('home');
        }

        return $this->render('security/change.html.twig');
    }

}