<?php

namespace App\Controller;

use App\Service\AdminService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    private AdminService $adminService;

    /**
     * AdminController constructor.
     * @param AdminService $adminService
     */
    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    /**
     * @Route("/admin", name="admin", methods={"GET"})
     */
    public function index(): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $model = $this->adminService->getDashboardData();

        return $this->render('admin/dashboard.html.twig', $model);
    }

    /**
     * @Route("/admin/vaccine", name="vaccine", methods={"GET"})
     */
    public function vaccineAction(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $selectedHospital = intval($request->request->get("hospital_input"));

        list($vaccines, $stock, $hospitalList, $vaccineList, $selectedHospital) = $this->adminService->getVaccineViewData($selectedHospital);

        return $this->render('admin/vaccine.html.twig', ["vaccineList" => $vaccines, "vaccineStock" => $stock, "hospitalSelection" => $hospitalList, "vaccineSelection" => $vaccineList, "selectedHospital" => $selectedHospital]);
    }

    /**
     * @Route("/admin/vaccine/add", name="add_vaccine", methods={"POST"})
     */
    public function addVaccineAction(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $name = filter_var($request->request->get("name_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $company = filter_var($request->request->get("company_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $risks = filter_var($request->request->get("risk_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $storage = filter_var($request->request->get("storage_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        $this->adminService->addVaccine($name, $company, $risks, $storage);

        return $this->redirectToRoute('vaccine');
    }

    /**
     * @Route("/admin/vaccine/stock", name="add_vaccine_stock", methods={"POST"})
     */
    public function addVaccineStockAction(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $vaccineId = intval($request->request->get("vaccine_input"));
        $hospitalId = intval($request->request->get("selected_hospital_input"));
        $stock = intval($request->request->get("stock_input"));

        $this->adminService->addVaccineStock($vaccineId, $hospitalId, $stock);

        $this->addFlash("notice", "STOCK_ADDED");

        return $this->redirectToRoute('vaccine', ["hospital_input" => $hospitalId]);
    }

    /**
     * @Route("/admin/hospital", name="hospital", methods={"GET"})
     */
    public function hospitalAction(): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $list = $this->adminService->getHospitalData();

        return $this->render('admin/hospital.html.twig', ["model" => $list]);
    }

    /**
     * @Route("/admin/hospital/add", name="add_hospital", methods={"POST"})
     */
    public function addHospitalAction(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $name = filter_var($request->request->get("name_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $address = filter_var($request->request->get("address_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $capacity = intval(filter_var($request->request->get("capacity_input"), FILTER_SANITIZE_SPECIAL_CHARS));
        $phone = filter_var($request->request->get("phone_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $email = filter_var($request->request->get("email_input"), FILTER_SANITIZE_SPECIAL_CHARS);

        $this->adminService->addHospital($name, $address, $capacity, $phone, $email);

        return $this->redirectToRoute("hospital");
    }

    /**
     * @Route("/admin/users", name="users", methods={"GET"})
     */
    public function usersAction(): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $userToEdit = null;
        $queryParam = $this->getv('edit', 'false');
        if ($queryParam !== 'false') {
            $userToEdit = $this->adminService->getUser(intval($queryParam));
        }

        list($users, $availableRoles) = $this->adminService->getUsers();

        return $this->render('admin/users.html.twig', ["users" => $users, "roleList" => $availableRoles, "userToEdit" => $userToEdit]);
    }

    function getv($key, $default = '', $data_type = '')
    {
        $param = ($_REQUEST[$key] ?? $default);

        if (!is_array($param) && $data_type == 'int') {
            $param = intval($param);
        }

        return $param;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/admin/users/add", name="add_user", methods={"POST"})
     */
    public function addUserAction(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $username = filter_var($request->request->get("username_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $fullName = filter_var($request->request->get("name_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $role = filter_var($request->request->get("role_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        $this->adminService->addUser($username, $fullName, $role);

        return $this->redirectToRoute('users');
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return Response
     * @Route("/admin/users/{userId}/delete", name="delete_user", requirements={"userId": "\d+"}, methods={"GET"})
     */
    public function deleteUser(Request $request, int $userId): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $this->adminService->deleteUser($userId);

        return $this->redirectToRoute('users');
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return Response
     * @Route("/admin/users/{userId}/edit", name="edit_user", requirements={"userId": "\d+"}, methods={"GET"})
     */
    public function editUser(Request $request, int $userId): Response
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");
        return $this->redirectToRoute('users', ["edit" => $userId]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/admin/users/update", name="modify_user", methods={"POST"})
     */
    public function modifyUser(Request $request): Response
    {
        $id = intval($request->request->get("id_input"));
        $newUsername = filter_var($request->request->get("username_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        if($id && $newUsername){
            $this->adminService->updateUser($id, $newUsername);
        }

        return $this->redirectToRoute('users');
    }

}
