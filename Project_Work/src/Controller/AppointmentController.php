<?php


namespace App\Controller;


use App\Entity\AppointmentEntity;
use App\Entity\PatientEntity;
use App\Models\HospitalSelectionModel;
use App\Models\SummaryModel;
use App\Models\VaccineSelectionModel;
use App\Repository\AppointmentRepository;
use App\Repository\HospitalRepository;
use App\Repository\PatientRepository;
use App\Repository\VaccineRepository;
use App\Repository\VaccineStockRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppointmentController extends AbstractController
{
    private PatientRepository $patientRepository;
    private HospitalRepository $hospitalRepository;
    private VaccineRepository $vaccineRepository;
    private VaccineStockRepository $vaccineStockRepository;
    private AppointmentRepository $appointmentRepository;

    /**
     * AppointmentController constructor.
     * @param PatientRepository $patientRepository
     * @param HospitalRepository $hospitalRepository
     * @param VaccineRepository $vaccineRepository
     * @param VaccineStockRepository $vaccineStockRepository
     * @param AppointmentRepository $appointmentRepository
     */
    public function __construct(PatientRepository $patientRepository, HospitalRepository $hospitalRepository, VaccineRepository $vaccineRepository, VaccineStockRepository $vaccineStockRepository, AppointmentRepository $appointmentRepository)
    {
        $this->patientRepository = $patientRepository;
        $this->hospitalRepository = $hospitalRepository;
        $this->vaccineRepository = $vaccineRepository;
        $this->vaccineStockRepository = $vaccineStockRepository;
        $this->appointmentRepository = $appointmentRepository;
    }

    /**
     * @Route("/appointment/basic/add", name="basic_info", methods={"POST"})
     */
    public function registerBasicInfoAction(Request $request): Response
    {
        $errorList = array();

        $taj = filter_var($request->request->get("taj_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $name = filter_var($request->request->get("name_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $age = filter_var($request->request->get("age_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $phone = filter_var($request->request->get("phone_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $knownDiseases = filter_var($request->request->get("diseases_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $address = filter_var($request->request->get("address_input"), FILTER_SANITIZE_SPECIAL_CHARS);

        $errorList = $this->validateBasicFormInputs($taj, $errorList, $name, $age, $phone, $address);

        $this->get('session')->set('taj', $taj);
        $this->get('session')->set('name', $name);
        $this->get('session')->set('age', $age);
        $this->get('session')->set('phone', $phone);
        $this->get('session')->set('knownDiseases', $knownDiseases);
        $this->get('session')->set('recipeName', $address);

        if (count($errorList) > 0) {
            foreach ($errorList as $notice) {
                $this->addFlash("notice", $notice);
            }
            return $this->redirectToRoute('home');
        }

        return $this->redirectToRoute('vaccine_view');
    }

    //https://github.com/pear/Validate_HU/blob/master/Validate/HU.php
    function ssn($ssn)
    {
        if (!preg_match("/^\d{9}$/", $ssn)) return false;

        $weights = array(3, 7);
        $sum = 0;
        for ($i = 0; $i < 8; $i++) {
            $sum += $weights[$i % 2] * intval(substr($ssn, $i, 1));
        }
        return substr($ssn, 8, 1) == ($sum % 10);
    }

    //https://phpisset.com/php-validate-phone-number
    function isValidTelephoneNumber(string $telephone, int $minDigits = 9, int $maxDigits = 14): bool
    {
        //remove white space, dots, hyphens and brackets
        $telephone = str_replace([' ', '.', '-', '(', ')', '+'], '', $telephone);

        //are we left with digits only?
        return $this->isDigits($telephone, $minDigits, $maxDigits);
    }

    //https://phpisset.com/php-validate-phone-number
    function isDigits(string $s, int $minDigits = 9, int $maxDigits = 14): bool
    {
        return preg_match('/^[0-9]{' . $minDigits . ',' . $maxDigits . '}\z/', $s);
    }

    /**
     * @Route("/appointment/vaccine", name="vaccine_view", methods={"GET", "POST"})
     */
    public function VaccineAction(Request $request): Response
    {
        $selectedHospital = $request->request->get("hospital_input");
        $selectedVaccine = filter_var($request->request->get("vaccine_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $selectedDate = filter_var($request->request->get("date_input"), FILTER_SANITIZE_SPECIAL_CHARS);
        $selectedSlot = filter_var($request->request->get("time_input"), FILTER_SANITIZE_SPECIAL_CHARS);

        list($timeSlots, $vaccineList, $hospitalList, $from, $to, $selectedHospital) = $this->getDateSelectionData($selectedHospital, $selectedVaccine, $selectedDate);

        $this->get('session')->set('selected_timeslot', $selectedSlot);
        $this->get('session')->set('selected_hospital', $selectedHospital);
        $this->get('session')->set('selected_vaccine', $selectedVaccine);
        $this->get('session')->set('selected_date', $selectedDate);

        if ($selectedSlot && $selectedHospital && $selectedVaccine && $selectedDate && isset($_POST['btnBook'])) {
            return $this->redirectToRoute('summary_view');
        }

        return $this->render('appointment/vaccine.html.twig', ["from" => $from, "to" => $to, "selectedDate" => $selectedDate, "vaccineSelection" => $vaccineList, "hospitalSelection" => $hospitalList, "timeSlots" => $timeSlots]);
    }

    /**
     * @Route("/appointment/summary", name="summary_view", methods={"GET"})
     */
    public function SummaryAction(): Response
    {
        $selectedHospital = intval($this->get('session')->get('selected_hospital'));
        $selectedVaccine = intval($this->get('session')->get('selected_vaccine'));
        $selectedDate = date_create_from_format('Y-m-d', $this->get('session')->get('selected_date'));
        $selectedSlot = intval($this->get('session')->get('selected_timeslot'));;

        $hospital = $this->hospitalRepository->findById($selectedHospital);
        $vaccine = $this->vaccineRepository->findById($selectedVaccine);

        $patient = new PatientEntity(
            $this->get('session')->get('taj'),
            $this->get('session')->get('name'),
            $this->get('session')->get('age'),
            $this->get('session')->get('phone'),
            $this->get('session')->get('knownDiseases'),
            $this->get('session')->get('recipeName'));

        $this->patientRepository->add($patient);

        $this->vaccineStockRepository->decreaseStock($selectedHospital, $selectedVaccine);

        $this->appointmentRepository->add(new AppointmentEntity($patient, $hospital, $vaccine, $selectedDate->setTime($selectedSlot, 0)));

        return $this->render('appointment/summary.html.twig', ["model" => new SummaryModel($patient->getTaj(), $patient->getFullName(), $patient->getAge(), $patient->getPhone(), $patient->getKnownDiseases(), $patient->getAddress(), $hospital->getName(), $vaccine->getName(), $selectedDate, $selectedSlot)]);
    }

    /**
     * @param $taj
     * @param array $errorList
     * @param $name
     * @param $age
     * @param $phone
     * @param $address
     * @return array
     */
    private function validateBasicFormInputs($taj, array $errorList, $name, $age, $phone, $address): array
    {
        if (!$this->ssn($taj)) {
            array_push($errorList, "TAJ number is invalid");
        } else {
            $patient = $this->patientRepository->findById($taj);
            if ($patient) {
                array_push($errorList, "Already registered");
            }
        }
        if (strlen($name) == 0) {
            array_push($errorList, "Please enter your name");
        }
        if (is_nan($age) || $age < 18 || $age > 150) {
            array_push($errorList, "Please enter your valid age (you can't get a vaccine if you are under 18)");
        }
        if (!$this->isValidTelephoneNumber($phone)) {
            array_push($errorList, "Please enter your valid phone number");
        }
        if (strlen($address) == 0) {
            array_push($errorList, "Please enter your address");
        }
        return $errorList;
    }

    /**
     * @param $selectedHospital
     * @param $selectedVaccine
     * @param $selectedDate
     * @return array
     */
    private function getDateSelectionData($selectedHospital, $selectedVaccine, $selectedDate): array
    {
        $hospital = null;
        $timeSlots = array();
        $vaccineList = array();
        $hospitalList = array();
        $from = new \DateTime('now');
        $to = new \DateTime('now');
        $to->modify('+31 day');

        $hospitalEntities = $this->hospitalRepository->list();

        if (!$selectedHospital && $hospitalEntities > 0) {
            $selectedHospital = $hospitalEntities[0]->getHospitalId();
        }

        $vaccineStock = $this->vaccineStockRepository->listByHospital(intval($selectedHospital));

        foreach ($hospitalEntities as $hospitalEntity) {
            array_push($hospitalList, new HospitalSelectionModel($hospitalEntity->getHospitalId(), $hospitalEntity->getName(), $hospitalEntity->getHospitalId() == $selectedHospital));
            if ($hospitalEntity->getHospitalId() == $selectedHospital) {
                $hospital = $hospitalEntity;
            }
        }
        foreach ($vaccineStock as $stock) {
            if ($stock->getStock() > 0) {
                array_push($vaccineList, new VaccineSelectionModel($stock->getVaccine()->getVaccineId(), $stock->getVaccine()->getName(), $stock->getVaccine()->getVaccineId() == intval($selectedVaccine)));
            }
        }

        if ($selectedDate) {
            $appointmentCount = array();
            $date = date_create_from_format('Y-m-d', $selectedDate);
            $appointments = $this->appointmentRepository->listByBookedDate($date);

            foreach ($appointments as $appointment) {
                $hour = $appointment->format('H');
                if (array_key_exists($hour, $appointmentCount)) {
                    $appointmentCount[$hour]++;
                } else {
                    $appointmentCount[$hour] = 1;
                }
            }

            for ($i = 8; $i < 19; $i++) {
                if (!array_key_exists("{$i}", $appointmentCount) || $appointmentCount["{$i}"] < $hospital->getCapacityPerHour()) {
                    array_push($timeSlots, $i);
                }
            }
        }
        return array($timeSlots, $vaccineList, $hospitalList, $from, $to, $selectedHospital);
    }

}