<?php


namespace App\Models;


class SummaryModel
{
    /** @var string */
    private $taj;
    /** @var string */
    private $name;
    /** @var int */
    private $age;
    /** @var string */
    private $phone;
    /** @var string */
    private $knownDiseases;
    /** @var string */
    private $address;
    /** @var string */
    private $hospital;
    /** @var string */
    private $vaccine;
    /** @var \DateTime */
    private $date;
    /** @var string */
    private $slot;

    /**
     * SummaryModel constructor.
     * @param string $taj
     * @param string $name
     * @param int $age
     * @param string $phone
     * @param string $knownDiseases
     * @param string $address
     * @param string $hospital
     * @param string $vaccine
     * @param \DateTime $date
     * @param string $slot
     */
    public function __construct(string $taj, string $name, int $age, string $phone, string $knownDiseases, string $address, string $hospital, string $vaccine, \DateTime $date, string $slot)
    {
        $this->taj = $taj;
        $this->name = $name;
        $this->age = $age;
        $this->phone = $phone;
        $this->knownDiseases = $knownDiseases;
        $this->address = $address;
        $this->hospital = $hospital;
        $this->vaccine = $vaccine;
        $this->date = $date;
        $this->slot = $slot;
    }

    /**
     * @return string
     */
    public function getTaj(): string
    {
        return $this->taj;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getKnownDiseases(): string
    {
        return $this->knownDiseases;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getHospital(): string
    {
        return $this->hospital;
    }

    /**
     * @return string
     */
    public function getVaccine(): string
    {
        return $this->vaccine;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getSlot(): string
    {
        return $this->slot;
    }
}