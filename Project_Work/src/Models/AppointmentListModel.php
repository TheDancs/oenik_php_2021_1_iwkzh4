<?php


namespace App\Models;


class AppointmentListModel
{
    /** @var int */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $taj;
    /** @var int */
    private $age;
    /** @var string */
    private $address;
    /** @var string */
    private $knownDiseases;
    /** @var string */
    private $vaccine;
    /** @var bool */
    private $received;

    /**
     * AppointmentListModel constructor.
     * @param int $id
     * @param string $name
     * @param string $taj
     * @param int $age
     * @param string $address
     * @param string $knownDiseases
     * @param string $vaccine
     * @param bool $received
     */
    public function __construct(int $id, string $name, string $taj, int $age, string $address, string $knownDiseases, string $vaccine, bool $received)
    {
        $this->id = $id;
        $this->name = $name;
        $this->taj = $taj;
        $this->age = $age;
        $this->address = $address;
        $this->knownDiseases = $knownDiseases;
        $this->vaccine = $vaccine;
        $this->received = $received;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTaj(): string
    {
        return $this->taj;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getKnownDiseases(): string
    {
        return $this->knownDiseases;
    }

    /**
     * @return string
     */
    public function getVaccine(): string
    {
        return $this->vaccine;
    }

    /**
     * @return bool
     */
    public function isReceived(): bool
    {
        return $this->received;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


}