<?php


namespace App\Models;


class VaccineSelectionModel
{
    private int $id;
    private string $name;
    private bool $selected;

    /**
     * VaccineSelectionModel constructor.
     * @param int $id
     * @param string $name
     * @param bool $selected
     */
    public function __construct(int $id, string $name, bool $selected = false)
    {
        $this->id = $id;
        $this->name = $name;
        $this->selected = $selected;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isSelected(): bool
    {
        return $this->selected;
    }
}