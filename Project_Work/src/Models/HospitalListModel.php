<?php


namespace App\Models;


class HospitalListModel
{
    /** @var HospitalModel[] */
    private $hotelList;

    /**
     * HospitalListModel constructor.
     * @param HospitalModel[] $hotelList
     */
    public function __construct(array $hotelList)
    {
        $this->hotelList = $hotelList;
    }

    /**
     * @return HospitalModel[]
     */
    public function getHotelList(): array
    {
        return $this->hotelList;
    }
}