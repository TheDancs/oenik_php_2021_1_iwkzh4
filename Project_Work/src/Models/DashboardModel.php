<?php


namespace App\Models;


class DashboardModel
{
    private $registeredAppointmentsToday;
    private $totalAvailableVaccines;
    private $appointmentsToday;

    /**
     * DashboardModel constructor.
     * @param $registeredAppointmentsToday
     * @param $totalAvailableVaccines
     * @param $appointmentsToday
     */
    public function __construct($registeredAppointmentsToday, $totalAvailableVaccines, $appointmentsToday)
    {
        $this->registeredAppointmentsToday = $registeredAppointmentsToday;
        $this->totalAvailableVaccines = $totalAvailableVaccines;
        $this->appointmentsToday = $appointmentsToday;
    }

    /**
     * @return mixed
     */
    public function getRegisteredAppointmentsToday()
    {
        return $this->registeredAppointmentsToday;
    }

    /**
     * @param mixed $registeredAppointmentsToday
     * @return DashboardModel
     */
    public function setRegisteredAppointmentsToday($registeredAppointmentsToday)
    {
        $this->registeredAppointmentsToday = $registeredAppointmentsToday;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalAvailableVaccines()
    {
        return $this->totalAvailableVaccines;
    }

    /**
     * @param mixed $totalAvailableVaccines
     * @return DashboardModel
     */
    public function setTotalAvailableVaccines($totalAvailableVaccines)
    {
        $this->totalAvailableVaccines = $totalAvailableVaccines;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAppointmentsToday()
    {
        return $this->appointmentsToday;
    }

    /**
     * @param mixed $appointmentsToday
     * @return DashboardModel
     */
    public function setAppointmentsToday($appointmentsToday)
    {
        $this->appointmentsToday = $appointmentsToday;
        return $this;
    }

}