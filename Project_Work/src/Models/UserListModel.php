<?php


namespace App\Models;


class UserListModel
{
    private int $id;
    private string $username;
    private array $roles;
    private \DateTime $createdAt;

    /**
     * UserListModel constructor.
     * @param int $id
     * @param string $username
     * @param array $roles
     * @param \DateTime $createdAt
     */
    public function __construct(int $id, string $username, array $roles, \DateTime $createdAt)
    {
        $this->id = $id;
        $this->username = $username;
        $this->roles = $roles;
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }


}