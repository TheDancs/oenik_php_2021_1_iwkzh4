<?php


namespace App\Models;


class HospitalSelectionModel
{
    private int $id;
    private string $name;
    private bool $isSelected;

    /**
     * HospitalSelectionModel constructor.
     * @param int $id
     * @param string $name
     * @param bool $isSelected
     */
    public function __construct(int $id, string $name, bool $isSelected)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isSelected = $isSelected;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isSelected(): bool
    {
        return $this->isSelected;
    }
}