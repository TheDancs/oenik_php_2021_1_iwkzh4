<?php


namespace App\Models;


class HospitalModel
{
    private $name;
    private $address;
    private $capacityPerHour;
    private $phone;

    /**
     * HospitalListModel constructor.
     * @param $name
     * @param $address
     * @param $capacityPerHour
     * @param $phone
     */
    public function __construct($name, $address, $capacityPerHour, $phone)
    {
        $this->name = $name;
        $this->address = $address;
        $this->capacityPerHour = $capacityPerHour;
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getCapacityPerHour()
    {
        return $this->capacityPerHour;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }
}