<?php


namespace App\Models;


class VaccineListModel
{
    /** @var string */
    private $name;
    /** @var string */
    private $company;
    /** @var \DateTime */
    private $created;

    /**
     * VaccineListModel constructor.
     * @param string $name
     * @param string $company
     * @param \DateTime $created
     */
    public function __construct(string $name, string $company, \DateTime $created)
    {
        $this->name = $name;
        $this->company = $company;
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }


}