<?php


namespace App\Models;


class VaccineStockModel
{
    /** @var string */
    private $name;
    /** @var int */
    private $stock;
    /** @var \DateTime */
    private $updated;

    /**
     * VaccineStockModel constructor.
     * @param string $name
     * @param int $stock
     * @param \DateTime $updated
     */
    public function __construct(string $name, int $stock, \DateTime $updated)
    {
        $this->name = $name;
        $this->stock = $stock;
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }


}