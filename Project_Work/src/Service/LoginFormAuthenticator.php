<?php


namespace App\Service;


use App\Entity\UserEntity;
use App\Models\LoginModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private EntityManagerInterface $entityManager;
    private RouterInterface $router;
    private UserPasswordEncoderInterface $encoder;
    private FormFactoryInterface $formFactory;

    /**
     * LoginFormAuthenticator constructor.
     * @param EntityManagerInterface $entityManager
     * @param RouterInterface $router
     * @param UserPasswordEncoderInterface $encoder
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(EntityManagerInterface $entityManager, RouterInterface $router, UserPasswordEncoderInterface $encoder, FormFactoryInterface $formFactory)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->encoder = $encoder;
        $this->formFactory = $formFactory;
    }

    public function supports(Request $request)
    {
        return $request->attributes->get('_route') === 'app_login' && $request->isMethod("POST");
    }

    public function getCredentials(Request $request)
    {
        $userName = filter_var($request->request->get("username_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $password = filter_var($request->request->get("password_input"), FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        if (!$userName || !$password) {
            return throw new InvalidCsrfTokenException("INVALID FORM");
        }

        return new LoginModel($userName, $password);
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var LoginModel $credentials */
        $user = $this->entityManager->getRepository(UserEntity::class)->findOneBy(["username" => $credentials->getUsername()]);
        if (!$user) throw new CustomUserMessageAuthenticationException("BAD USERNAME");
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        /** @var LoginModel $credentials */
        return $this->encoder->isPasswordValid($user, $credentials->getPassword());
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        /** @var UserEntity $user */

        $targetPath = $this->getTargetPath($request->getSession(), $providerKey);
        $username = $token->getUsername();
        $user = $this->entityManager->getRepository(UserEntity::class)->findOneBy(["username" => $username]);
        if (!$user->isPwdChanged()) {
            return new RedirectResponse($this->router->generate("app_change_pwd"));
        }

        if ($targetPath) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->router->generate("home"));
    }

    protected function getLoginUrl()
    {
        return $this->router->generate("app_login");
    }
}