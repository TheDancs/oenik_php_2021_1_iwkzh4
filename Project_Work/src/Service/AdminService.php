<?php


namespace App\Service;


use App\Entity\HospitalEntity;
use App\Entity\VaccineEntity;
use App\Entity\VaccineStockEntity;
use App\Models\DashboardModel;
use App\Models\HospitalListModel;
use App\Models\HospitalSelectionModel;
use App\Models\UserListModel;
use App\Models\VaccineListModel;
use App\Models\VaccineSelectionModel;
use App\Models\VaccineStockModel;
use App\Repository\AppointmentRepository;
use App\Repository\HospitalRepository;
use App\Repository\VaccineRepository;
use App\Repository\VaccineStockRepository;

class AdminService
{
    private SecurityService $securityService;
    private VaccineRepository $vaccineRepository;
    private HospitalRepository $hospitalRepository;
    private AppointmentRepository $appointmentRepository;
    private VaccineStockRepository $vaccineStockRepository;

    /**
     * AdminService constructor.
     * @param SecurityService $securityService
     * @param VaccineRepository $vaccineRepository
     * @param HospitalRepository $hospitalRepository
     * @param AppointmentRepository $appointmentRepository
     * @param VaccineStockRepository $vaccineStockRepository
     */
    public function __construct(SecurityService $securityService, VaccineRepository $vaccineRepository, HospitalRepository $hospitalRepository, AppointmentRepository $appointmentRepository, VaccineStockRepository $vaccineStockRepository)
    {
        $this->securityService = $securityService;
        $this->vaccineRepository = $vaccineRepository;
        $this->hospitalRepository = $hospitalRepository;
        $this->appointmentRepository = $appointmentRepository;
        $this->vaccineStockRepository = $vaccineStockRepository;
    }

    /**
     * @return DashboardModel[]
     */
    public function getDashboardData(): array
    {
        $availableVaccines = 0;
        $appointmentsToday = 0;
        $newRegistrations = 0;

        $stock = $this->vaccineStockRepository->list();
        $registrations = $this->appointmentRepository->listByCreateDate(new \DateTime('now'));
        $appointments = $this->appointmentRepository->listByBookedDate(new \DateTime('now'));

        foreach ($stock as $vaccineStock) {
            $availableVaccines += $vaccineStock->getStock();
        }
        $newRegistrations = count($registrations);
        $appointmentsToday = count($appointments);

        $dashboardModel = new DashboardModel($newRegistrations, $availableVaccines, $appointmentsToday);
        return ["model" => $dashboardModel];
    }

    /**
     * @param int $selectedHospital
     * @return array
     */
    public function getVaccineViewData(int $selectedHospital): array
    {
        $vaccines = array();
        $stock = array();
        $hospitalList = array();
        $vaccineList = array();

        $hospitalEntities = $this->hospitalRepository->list();
        $vaccineEntities = $this->vaccineRepository->list();
        if (!$selectedHospital && count($hospitalEntities) > 0) {
            $selectedHospital = $hospitalEntities[0]->getHospitalId();
        }
        $stockEntities = $this->vaccineStockRepository->listByHospital($selectedHospital);

        foreach ($vaccineEntities as $vaccineEntity) {
            array_push($vaccines, new VaccineListModel($vaccineEntity->getName(), $vaccineEntity->getCompany(), $vaccineEntity->getCreatedAt()));
            array_push($vaccineList, new VaccineSelectionModel($vaccineEntity->getVaccineId(), $vaccineEntity->getName()));
        }
        foreach ($stockEntities as $stockEntity) {
            array_push($stock, new VaccineStockModel($stockEntity->getVaccine()->getName(), $stockEntity->getStock(), $stockEntity->getUpdated()));
        }
        foreach ($hospitalEntities as $hospitalEntity) {
            array_push($hospitalList, new HospitalSelectionModel($hospitalEntity->getHospitalId(), $hospitalEntity->getName(), $hospitalEntity->getHospitalId() == $selectedHospital));
        }

        return array($vaccines, $stock, $hospitalList, $vaccineList, $selectedHospital);
    }

    /**
     * @return HospitalListModel
     */
    public function getHospitalData(): HospitalListModel
    {
        return new HospitalListModel($this->hospitalRepository->list());
    }

    /**
     * @return array
     */
    public function getUsers(): array
    {
        $users = $this->securityService->listUsers();
        $availableRoles = $this->securityService->getAvailableRoles();

        return array($users, $availableRoles);
    }

    public function getUser(int $userId): UserListModel
    {
        return $this->securityService->getUser($userId);
    }

    public function addVaccine(string $name, string $company, string $risks, string $storage): bool
    {
        if ($name && $company) {
            $vaccine = new VaccineEntity($name, $company, $risks, $storage);
            $this->vaccineRepository->add($vaccine);

            return true;
        }

        return false;
    }

    public function addUser(string $username, string $fullName, string $role): bool
    {
        if ($username && strlen($username) > 4 && $fullName && $role) {
            $this->securityService->addUser($username, $fullName, "default", $role);
            return true;
        }

        return false;
    }

    public function updateUser(int $id, string $newUsername): bool
    {
        return $this->securityService->updateUser($id, $newUsername);
    }

    public function deleteUser(int $userId): bool
    {
        return $this->securityService->deleteUser($userId);
    }

    public function addHospital(string $name, string $address, int $capacity, string $phone, string $email): bool
    {
        if ($name && $address && $capacity && $phone && $email) {
            $this->hospitalRepository->add(new HospitalEntity($name, $address, $capacity, $phone, $email));

            return true;
        }

        return false;
    }

    public function addVaccineStock(int $vaccineId, int $hospitalId, int $stock): bool
    {
        $vaccine = $this->vaccineRepository->findById($vaccineId);
        $hospital = $this->hospitalRepository->findById($hospitalId);

        if ($vaccine && $hospital) {
            $vaccineStock = new VaccineStockEntity($hospital, $vaccine, $stock);
            $this->vaccineStockRepository->addOrUpdate($vaccineStock);

            return true;
        }

        return false;
    }

}