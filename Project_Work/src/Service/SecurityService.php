<?php


namespace App\Service;


use App\Entity\UserEntity;
use App\Models\UserListModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityService
{
    private EntityManagerInterface $entityManager;
    private UserPasswordEncoderInterface $encoder;

    /**
     * SecurityService constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder)
    {
        $this->entityManager = $entityManager;
        $this->encoder = $encoder;
    }

    public function addUser(string $username, string $fullName, string $defaultPass, string $role = "ROLE_NURSE"): bool
    {
        if (in_array($role, $this->getAvailableRoles())) {
            $user = new UserEntity();
            $user->setFullName($fullName)
                ->setUsername($username)
                ->setRoles([$role])
                ->setPassword($this->encoder->encodePassword($user, $defaultPass));

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    /**
     * @return string[]
     */
    public function getAvailableRoles(): array
    {
        return ["ROLE_ADMIN", "ROLE_NURSE"];
    }

    public function deleteUser(int $userId): bool
    {
        /** @var UserEntity $user */
        $user = $this->entityManager->getRepository(UserEntity::class)->find($userId);
        if ($user) {
            if ($user->getRoles() == ["ROLE_ADMIN"]) {
                //Imp: check if its the only admin (JSON array match is tricky)
                return false;
            }

            $this->entityManager->remove($user);
            $this->entityManager->flush();

            return true;
        }
        return false;
    }

    public function checkPassword(string $username, string $clearPass): bool
    {
        $user = $this->entityManager->getRepository(UserEntity::class)->findOneBy(["username" => $username]);
        if (!$user) return false;
        return $this->encoder->isPasswordValid($user, $clearPass);
    }

    public function changePassword(string $username, string $newPasswd): bool
    {
        /** @var UserEntity $user */
        $user = $this->entityManager->getRepository(UserEntity::class)->findOneBy(["username" => $username]);
        if (!$user) return false;

        $user->setPassword($this->encoder->encodePassword($user, $newPasswd))
            ->setPwdChanged(true);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @return UserListModel[]
     */
    public function listUsers(): array
    {
        /** @var UserEntity[] $users */
        /** @var UserEntity $user */
        $users = array();
        $userEntities = $this->entityManager->getRepository(UserEntity::class)->findAll();
        foreach ($userEntities as $user) {
            array_push($users, new UserListModel($user->getId(), $user->getUsername(), $user->getRoles(), $user->getCreatedAt()));
        }

        return $users;
    }

    public function getUser(int $userId): ?UserListModel
    {
        $user = $this->entityManager->getRepository(UserEntity::class)->find($userId);
        if ($user) {
            return new UserListModel($user->getId(), $user->getUsername(), $user->getRoles(), $user->getCreatedAt());
        }
        return null;
    }

    public function updateUser(int $id, string $newUsername): bool
    {
        $user = $this->entityManager->getRepository(UserEntity::class)->find($id);
        if (!$user) return false;

        $user->setUsername($newUsername);

        $this->entityManager->flush();

        return true;
    }

}