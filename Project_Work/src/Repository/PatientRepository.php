<?php


namespace App\Repository;


use App\Entity\PatientEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class PatientRepository
{
    private ObjectRepository $repository;
    private EntityManagerInterface $entityManager;

    /**
     * PatientRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(PatientEntity::class);
        $this->entityManager = $entityManager;
    }

    public function add(PatientEntity $patientEntity): void
    {
        $this->entityManager->persist($patientEntity);
        $this->entityManager->flush();
    }

    public function list(): array
    {
        return $this->repository->findAll();
    }

    public function findById(string $taj): ?PatientEntity
    {
        return $this->repository->find($taj);
    }

}