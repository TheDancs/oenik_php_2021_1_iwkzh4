<?php


namespace App\Repository;


use App\Entity\HospitalEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class HospitalRepository
{
    private ObjectRepository $repository;
    private EntityManagerInterface $entityManager;

    /**
     * HospitalRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(HospitalEntity::class);
    }

    public function findById(int $hospitalId): HospitalEntity
    {
        return $this->repository->find($hospitalId);
    }

    public function list(): array
    {
        return $this->repository->findAll();
    }

    public function add(HospitalEntity $hospitalEntity)
    {
        $this->entityManager->persist($hospitalEntity);
        $this->entityManager->flush();
    }

}