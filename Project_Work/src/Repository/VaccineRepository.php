<?php


namespace App\Repository;


use App\Entity\VaccineEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class VaccineRepository
{
    private ObjectRepository $repository;
    private EntityManagerInterface $entityManager;

    /**
     * VaccineRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(VaccineEntity::class);
        $this->entityManager = $entityManager;
    }

    public function add(VaccineEntity $vaccineEntity): void
    {
        $this->entityManager->persist($vaccineEntity);
        $this->entityManager->flush();
    }

    public function list(): array
    {
        return $this->repository->findAll();
    }

    public function findById(int $vaccineId): VaccineEntity
    {
        return $this->repository->find($vaccineId);
    }
}