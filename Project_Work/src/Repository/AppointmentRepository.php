<?php


namespace App\Repository;


use App\Entity\AppointmentEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class AppointmentRepository
{
    private ObjectRepository $repository;
    private EntityManagerInterface $entityManager;

    /**
     * AppointmentRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(AppointmentEntity::class);
        $this->entityManager = $entityManager;
    }

    public function add(AppointmentEntity $appointmentEntity): void
    {
        $this->entityManager->persist($appointmentEntity);
        $this->entityManager->flush();
    }

    public function listByHospital(int $hospitalId): array
    {
        return $this->repository->findBy(array('hospital' => $hospitalId));
    }

    public function listByCreateDate(\DateTime $date): array
    {
        $from = new \DateTime($date->format("Y-m-d") . " 00:00:00");
        $to = new \DateTime($date->format("Y-m-d") . " 23:59:59");

        return $this->entityManager->getRepository(AppointmentEntity::class)->createQueryBuilder("e")
            ->andWhere('e.created_at BETWEEN :from AND :to')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->getQuery()
            ->getResult();
    }

    public function listByBookedDate(\DateTime $date): array
    {
        $from = new \DateTime($date->format("Y-m-d") . " 00:00:00");
        $to = new \DateTime($date->format("Y-m-d") . " 23:59:59");

        return $this->entityManager->getRepository(AppointmentEntity::class)->createQueryBuilder("e")
            ->andWhere('e.booked_date BETWEEN :from AND :to')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->getQuery()
            ->getResult();
    }

    public function listByBookedDateAndHospital(\DateTime $date, int $hospitalId): array
    {
        $from = new \DateTime($date->format("Y-m-d") . " 00:00:00");
        $to = new \DateTime($date->format("Y-m-d") . " 23:59:59");

        return $this->entityManager->getRepository(AppointmentEntity::class)->createQueryBuilder("e")
            ->andWhere('e.booked_date BETWEEN :from AND :to AND e.hospital = :hospitalId')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('hospitalId', $hospitalId)
            ->getQuery()
            ->getResult();
    }

    public function findById(int $appointmentId): AppointmentEntity
    {
        return $this->repository->find($appointmentId);
    }

    public function updateEntity(AppointmentEntity $appointmentEntity): void
    {
        $this->entityManager->flush();
    }
}