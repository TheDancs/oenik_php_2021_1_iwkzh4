<?php


namespace App\Repository;


use App\Entity\VaccineStockEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class VaccineStockRepository
{
    private ObjectRepository $repository;
    private EntityManagerInterface $entityManager;

    /**
     * VaccineStockRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(VaccineStockEntity::class);
        $this->entityManager = $entityManager;
    }

    public function addOrUpdate(VaccineStockEntity $vaccineStockEntity): void
    {
        $stock = $this->findById($vaccineStockEntity->getHospital()->getHospitalId(), $vaccineStockEntity->getVaccine()->getVaccineId());
        if ($stock) {
            $query = $this->entityManager->getRepository(VaccineStockEntity::class)->createQueryBuilder("c")
                ->update()
                ->set("c.stock", "c.stock+{$vaccineStockEntity->getStock()}")
                ->where("c.vaccine = :vaccineId AND c.hospital = :hospitalId")
                ->setParameter("hospitalId", $vaccineStockEntity->getHospital()->getHospitalId())
                ->setParameter('vaccineId', $vaccineStockEntity->getVaccine()->getVaccineId())
                ->getQuery();

            $query->execute();
        } else {
            $this->entityManager->persist($vaccineStockEntity);
            $this->entityManager->flush();
        }
    }

    public function findById(int $hospitalId, int $vaccineId): ?VaccineStockEntity
    {
        $entities = $this->repository->findBy(array('hospital' => $hospitalId, 'vaccine' => $vaccineId));
        if ($entities && count($entities) > 0) {
            return $entities[0];
        }

        return null;
    }

    public function decreaseStock(int $hospitalId, int $vaccineId): void
    {
        $query = $this->entityManager->getRepository(VaccineStockEntity::class)->createQueryBuilder("c")
            ->update()
            ->set("c.stock", "c.stock-1")
            ->where("c.vaccine = :vaccineId AND c.hospital = :hospitalId")
            ->setParameter("hospitalId", $hospitalId)
            ->setParameter('vaccineId', $vaccineId)
            ->getQuery();

        $query->execute();
    }

    public function listByHospital(int $hospitalId): array
    {
        return $this->repository->findBy(array('hospital' => $hospitalId));
    }

    public function list(): array
    {
        return $this->repository->findAll();
    }
}