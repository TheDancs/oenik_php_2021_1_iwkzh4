<?php


namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class VaccineEntity
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="vaccines")
 * @ORM\HasLifecycleCallbacks
 */
class VaccineEntity
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue (strategy="AUTO")
     */
    private $vaccine_id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $company;

    /**
     * @var string
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private $risk_diseases;

    /**
     * @var string
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $storage_conditions;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @var ArrayCollection|VaccineStockEntity[]
     * @ORM\OneToMany(targetEntity="App\Entity\VaccineStockEntity", mappedBy="vaccine")
     */
    private $vaccineStock;

    /**
     * @var ArrayCollection|AppointmentEntity
     * @ORM\OneToMany (targetEntity="App\Entity\AppointmentEntity", mappedBy="vaccine")
     */
    private $appointments;

    /**
     * VaccineEntity constructor.
     * @param string $name
     * @param string $company
     * @param string $risk_diseases
     * @param string $storage_conditions
     */
    public function __construct(string $name, string $company, string $risk_diseases, string $storage_conditions)
    {
        $this->name = $name;
        $this->company = $company;
        $this->risk_diseases = $risk_diseases;
        $this->storage_conditions = $storage_conditions;
    }

    /**
     * @return int
     */
    public function getVaccineId(): int
    {
        return $this->vaccine_id;
    }

    /**
     * @param int $vaccine_id
     * @return VaccineEntity
     */
    public function setVaccineId(int $vaccine_id): VaccineEntity
    {
        $this->vaccine_id = $vaccine_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return VaccineEntity
     */
    public function setName(string $name): VaccineEntity
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @param string $company
     * @return VaccineEntity
     */
    public function setCompany(string $company): VaccineEntity
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return string
     */
    public function getRiskDiseases(): string
    {
        return $this->risk_diseases;
    }

    /**
     * @param string $risk_diseases
     * @return VaccineEntity
     */
    public function setRiskDiseases(string $risk_diseases): VaccineEntity
    {
        $this->risk_diseases = $risk_diseases;
        return $this;
    }

    /**
     * @return string
     */
    public function getStorageConditions(): string
    {
        return $this->storage_conditions;
    }

    /**
     * @param string $storage_conditions
     * @return VaccineEntity
     */
    public function setStorageConditions(string $storage_conditions): VaccineEntity
    {
        $this->storage_conditions = $storage_conditions;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateTimestamps()
    {
        $this->created_at = new \DateTime('now');
    }
}