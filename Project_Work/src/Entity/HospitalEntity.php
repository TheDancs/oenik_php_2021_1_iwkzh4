<?php


namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class HospitalEntity
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="hospitals")
 */
class HospitalEntity
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue (strategy="AUTO")
     */
    private $hospital_id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    private $address;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $capacity_per_hour;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var ArrayCollection|VaccineStockEntity[]
     * @ORM\OneToMany(targetEntity="App\Entity\VaccineStockEntity", mappedBy="hospital")
     */
    private $vaccineStock;

    /**
     * @var ArrayCollection|AppointmentEntity
     * @ORM\OneToMany (targetEntity="App\Entity\AppointmentEntity", mappedBy="hospital")
     */
    private $appointments;

    /**
     * HospitalEntity constructor.
     * @param string $name
     * @param string $address
     * @param int $capacity_per_hour
     * @param string $phone
     * @param string $email
     */
    public function __construct(string $name, string $address, int $capacity_per_hour, string $phone, string $email)
    {
        $this->name = $name;
        $this->address = $address;
        $this->capacity_per_hour = $capacity_per_hour;
        $this->phone = $phone;
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getHospitalId(): int
    {
        return $this->hospital_id;
    }

    /**
     * @param int $hospital_id
     * @return HospitalEntity
     */
    public function setHospitalId(int $hospital_id): HospitalEntity
    {
        $this->hospital_id = $hospital_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return HospitalEntity
     */
    public function setName(string $name): HospitalEntity
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return HospitalEntity
     */
    public function setAddress(string $address): HospitalEntity
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int
     */
    public function getCapacityPerHour(): int
    {
        return $this->capacity_per_hour;
    }

    /**
     * @param int $capacity_per_hour
     * @return HospitalEntity
     */
    public function setCapacityPerHour(int $capacity_per_hour): HospitalEntity
    {
        $this->capacity_per_hour = $capacity_per_hour;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return HospitalEntity
     */
    public function setPhone(string $phone): HospitalEntity
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return HospitalEntity
     */
    public function setEmail(string $email): HospitalEntity
    {
        $this->email = $email;
        return $this;
    }


}