<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * Class VaccineStockEntity
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="stocks")
 * @ORM\HasLifecycleCallbacks
 */
class VaccineStockEntity
{
    /**
     * @var HospitalEntity
     * @Id
     * @ManyToOne(targetEntity="App\Entity\HospitalEntity")
     * @ORM\JoinColumn(name="hospital", referencedColumnName="hospital_id", onDelete="CASCADE")
     */
    private $hospital;

    /**
     * @var VaccineEntity
     * @Id
     * @ORM\ManyToOne(targetEntity="App\Entity\VaccineEntity")
     * @ORM\JoinColumn(name="vaccine", referencedColumnName="vaccine_id", onDelete="CASCADE")
     */
    private $vaccine;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $stock;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $updated;

    /**
     * VaccineStockEntity constructor.
     * @param HospitalEntity $hospital
     * @param VaccineEntity $vaccine
     * @param int $stock
     */
    public function __construct(HospitalEntity $hospital, VaccineEntity $vaccine, int $stock)
    {
        $this->hospital = $hospital;
        $this->vaccine = $vaccine;
        $this->stock = $stock;
    }

    /**
     * @return HospitalEntity
     */
    public function getHospital(): HospitalEntity
    {
        return $this->hospital;
    }

    /**
     * @param HospitalEntity $hospital
     * @return VaccineStockEntity
     */
    public function setHospital(HospitalEntity $hospital): VaccineStockEntity
    {
        $this->hospital = $hospital;
        return $this;
    }

    /**
     * @return VaccineEntity
     */
    public function getVaccine(): VaccineEntity
    {
        return $this->vaccine;
    }

    /**
     * @param VaccineEntity $vaccine
     * @return VaccineStockEntity
     */
    public function setVaccine(VaccineEntity $vaccine): VaccineStockEntity
    {
        $this->vaccine = $vaccine;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return VaccineStockEntity
     */
    public function setStock(int $stock): VaccineStockEntity
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps()
    {
        $this->updated = new \DateTime('now');
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

}