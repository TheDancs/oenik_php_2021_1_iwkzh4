<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class AppointmentEntity
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="appointments")
 * @ORM\HasLifecycleCallbacks
 */
class AppointmentEntity
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue (strategy="AUTO")
     */
    private int $appointment_id;

    /**
     * @var PatientEntity
     * @ORM\ManyToOne(targetEntity="App\Entity\PatientEntity", inversedBy="appointments")
     * @ORM\JoinColumn(name="patient", referencedColumnName="taj", onDelete="CASCADE")
     */
    private PatientEntity $patient;

    /**
     * @var HospitalEntity
     * @ORM\ManyToOne(targetEntity="App\Entity\HospitalEntity", inversedBy="appointments")
     * @ORM\JoinColumn(name="hospital", referencedColumnName="hospital_id", onDelete="CASCADE")
     */
    private HospitalEntity $hospital;

    /**
     * @var VaccineEntity
     * @ORM\ManyToOne(targetEntity="App\Entity\VaccineEntity", inversedBy="appointments")
     * @ORM\JoinColumn(name="vaccine", referencedColumnName="vaccine_id", onDelete="CASCADE")
     */
    private VaccineEntity $vaccine;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private \DateTime $booked_date;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private bool $received;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private \DateTime $created_at;

    /**
     * AppointmentEntity constructor.
     * @param PatientEntity $patient
     * @param HospitalEntity $hospital
     * @param VaccineEntity $vaccine
     * @param \DateTime $booked_date
     */
    public function __construct(PatientEntity $patient, HospitalEntity $hospital, VaccineEntity $vaccine, \DateTime $booked_date, bool $received = false)
    {
        $this->patient = $patient;
        $this->hospital = $hospital;
        $this->vaccine = $vaccine;
        $this->booked_date = $booked_date;
        $this->received = $received;
    }

    /**
     * @return bool
     */
    public function isReceived(): bool
    {
        return $this->received;
    }

    /**
     * @param bool $received
     * @return AppointmentEntity
     */
    public function setReceived(bool $received): AppointmentEntity
    {
        $this->received = $received;
        return $this;
    }

    /**
     * @return int
     */
    public function getAppointmentId(): int
    {
        return $this->appointment_id;
    }

    /**
     * @param int $appointment_id
     * @return AppointmentEntity
     */
    public function setAppointmentId(int $appointment_id): AppointmentEntity
    {
        $this->appointment_id = $appointment_id;
        return $this;
    }

    /**
     * @return PatientEntity
     */
    public function getPatient(): PatientEntity
    {
        return $this->patient;
    }

    /**
     * @param PatientEntity $patient
     * @return AppointmentEntity
     */
    public function setPatient(PatientEntity $patient): AppointmentEntity
    {
        $this->patient = $patient;
        return $this;
    }

    /**
     * @return HospitalEntity
     */
    public function getHospital(): HospitalEntity
    {
        return $this->hospital;
    }

    /**
     * @param HospitalEntity $hospital
     * @return AppointmentEntity
     */
    public function setHospital(HospitalEntity $hospital): AppointmentEntity
    {
        $this->hospital = $hospital;
        return $this;
    }

    /**
     * @return VaccineEntity
     */
    public function getVaccine(): VaccineEntity
    {
        return $this->vaccine;
    }

    /**
     * @param VaccineEntity $vaccine
     * @return AppointmentEntity
     */
    public function setVaccine(VaccineEntity $vaccine): AppointmentEntity
    {
        $this->vaccine = $vaccine;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBookedDate(): \DateTime
    {
        return $this->booked_date;
    }

    /**
     * @param \DateTime $booked_date
     * @return AppointmentEntity
     */
    public function setBookedDate(\DateTime $booked_date): AppointmentEntity
    {
        $this->booked_date = $booked_date;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateTimestamps()
    {
        $this->created_at = new \DateTime('now');
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

}