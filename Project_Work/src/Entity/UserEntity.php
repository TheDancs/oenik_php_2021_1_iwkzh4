<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserEntity
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks
 */
class UserEntity implements UserInterface
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private int $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private string $full_name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     */
    private string $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private string $password;

    /**
     * @var string[]
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $pwdChanged = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_at;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->full_name;
    }

    /**
     * @param string $full_name
     * @return UserEntity
     */
    public function setFullName(string $full_name): UserEntity
    {
        $this->full_name = $full_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UserEntity
     */
    public function setPassword(string $password): UserEntity
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return UserEntity
     */
    public function setUsername(string $username): UserEntity
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param string[] $roles
     * @return UserEntity
     */
    public function setRoles(array $roles): UserEntity
    {
        $this->roles = $roles;
        return $this;
    }

    public function getSalt()
    {
        //salt is built in
        return null;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return bool
     */
    public function isPwdChanged(): bool
    {
        return $this->pwdChanged;
    }

    /**
     * @param bool $pwdChanged
     * @return UserEntity
     */
    public function setPwdChanged(bool $pwdChanged): UserEntity
    {
        $this->pwdChanged = $pwdChanged;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateTimestamps()
    {
        $this->created_at = new \DateTime('now');
    }
}