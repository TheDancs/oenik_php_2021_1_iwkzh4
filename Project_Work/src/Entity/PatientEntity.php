<?php


namespace App\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class PatientEntity
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="patients")
 */
class PatientEntity
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=15, nullable=false, unique=true)
     */
    private $taj;

    /**
     * @var string
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    private $full_name;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $age;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $known_diseases;

    /**
     * @var string
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    private $address;

    /**
     * @var ArrayCollection|AppointmentEntity
     * @ORM\OneToMany (targetEntity="App\Entity\AppointmentEntity", mappedBy="patient")
     */
    private $appointments;

    /**
     * PatientEntity constructor.
     * @param string $taj
     * @param string $full_name
     * @param int $age
     * @param string $phone
     * @param string $known_diseases
     * @param string $address
     */
    public function __construct(string $taj, string $full_name, int $age, string $phone, string $known_diseases, string $address)
    {
        $this->taj = $taj;
        $this->full_name = $full_name;
        $this->age = $age;
        $this->phone = $phone;
        $this->known_diseases = $known_diseases;
        $this->address = $address;
    }

    /**
     * @return AppointmentEntity|ArrayCollection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * @param AppointmentEntity|ArrayCollection $appointments
     * @return PatientEntity
     */
    public function setAppointments($appointments)
    {
        $this->appointments = $appointments;
        return $this;
    }

    /**
     * @return string
     */
    public function getTaj(): string
    {
        return $this->taj;
    }

    /**
     * @param string $taj
     * @return PatientEntity
     */
    public function setTaj(string $taj): PatientEntity
    {
        $this->taj = $taj;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->full_name;
    }

    /**
     * @param string $full_name
     * @return PatientEntity
     */
    public function setFullName(string $full_name): PatientEntity
    {
        $this->full_name = $full_name;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return PatientEntity
     */
    public function setAge(int $age): PatientEntity
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return PatientEntity
     */
    public function setPhone(string $phone): PatientEntity
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getKnownDiseases(): string
    {
        return $this->known_diseases;
    }

    /**
     * @param string $known_diseases
     * @return PatientEntity
     */
    public function setKnownDiseases(string $known_diseases): PatientEntity
    {
        $this->known_diseases = $known_diseases;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return PatientEntity
     */
    public function setAddress(string $address): PatientEntity
    {
        $this->address = $address;
        return $this;
    }
}