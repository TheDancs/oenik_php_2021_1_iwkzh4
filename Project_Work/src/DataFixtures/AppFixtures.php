<?php


namespace App\DataFixtures;

use App\Entity\UserEntity;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture implements ContainerAwareInterface
{
    private UserPasswordEncoderInterface $encoder;


    /** @var ContainerInterface */
    private $container;

    /**
     * AppFixtures constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = $manager->getRepository(UserEntity::class)->findOneBy(["username" => "superAdmin"]);

        if (!$admin) {
            $admin = new UserEntity();
            $admin
                ->setUsername("superAdmin")
                ->setPassword($this->encoder->encodePassword($admin, "default"))
                ->setRoles(["ROLE_ADMIN"])
                ->setFullName("Default administrator");
            $manager->persist($admin);
            $manager->flush();
        }
    }

    /*
     *   php bin/console doctrine:database:create
     *   php bin/console doctrine:schema:drop --force --full-database
     *   php bin/console doctrine:schema:update --dump-sql
     *   php bin/console doctrine:schema:update --force
     *   php bin/console doctrine:fixtures:load --no-interaction -vvv
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
        $kernel = $this->container->get('kernel');
        if ($kernel) {
            $this->environment = $kernel->getEnvironment();
        }
    }
}