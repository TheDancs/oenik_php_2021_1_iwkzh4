<?php


namespace App\Tests;


use App\Repository\AppointmentRepository;
use App\Repository\HospitalRepository;
use App\Repository\VaccineRepository;
use App\Repository\VaccineStockRepository;
use App\Service\AdminService;
use App\Service\SecurityService;
use PHPUnit\Framework\TestCase;

final class AdminServiceTest extends TestCase
{
    private SecurityService $secService;
    private VaccineRepository $vaccineRepo;
    private HospitalRepository $hospitalRepo;
    private AppointmentRepository $appointmentRepo;
    private VaccineStockRepository $vaccineStockRepo;

    /** @test */
    public function AddVaccineMissingParamsTest()
    {


        $adminSvc = new AdminService($this->secService, $this->vaccineRepo, $this->hospitalRepo, $this->appointmentRepo, $this->vaccineStockRepo);

        $result = $adminSvc->addVaccine("", "", "", "");

        $this->assertEquals(false, $result);
    }

    /** @test */
    public function AddVaccineTest()
    {
        $adminSvc = new AdminService($this->secService, $this->vaccineRepo, $this->hospitalRepo, $this->appointmentRepo, $this->vaccineStockRepo);

        $result = $adminSvc->addVaccine("test", "test", "", "");

        $this->assertEquals(true, $result);
    }

    protected function setUp(): void
    {
        $this->secService = $this->createMock(SecurityService::class);
        $this->vaccineRepo = $this->createMock(VaccineRepository::class);
        $this->hospitalRepo = $this->createMock(HospitalRepository::class);
        $this->appointmentRepo = $this->createMock(AppointmentRepository::class);
        $this->vaccineStockRepo = $this->createMock(VaccineStockRepository::class);
    }
}